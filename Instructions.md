# Instructions pour l'examen final, (aussi en [anglais](http://pauillac.inria.fr/~maranget/MPRI/howto-exam.html))

L'examen se déroule le mercredi 3 mars, de 16h15 à 19h15. Il s'agit d'un
examen « à distance ». Le sujet sera disponible ici (en
[français](http://pauillac.inria.fr/~maranget/MPRI/exam-fr.pdf),
[anglais](http://pauillac.inria.fr/~maranget/MPRI/exam-en.pdf)) au début
de l'examen.

## Comment procéder

Vous composer dans un fichier texte et l'envoyer par courrier
électronique à
[luc.maranget@inria.fr](mailto:luc.maranget@inria.fr?subject=MyCopy)
comme pièce jointe.

Cependant on peut préférer écrire sur des feuilles de papier. C'est tout
à fait possible. Pour nous remettre votre copie, suivre la procédure
suivante:

1.  Composer sur papier, comme d'habitude.
2.  Scanner ou photographier et envoyer votre copie ainsi :
    a.  Numéroter toutes les pages comme 1/n, ..., n/n.
    b.  Bien vérifier que toutes les pages sont lisibles.
    c.  Envoyer les pages comme pièces jointes par courrier
        électronique
        à [luc.maranget@inria.fr](mailto:luc.maranget@inria.fr).
        Dans la mesure du possible envoyer un unique message avec toutes
        les pages en pièces jointes.
    d.  Conserver votre copie sur papier.

## Instructions supplémentaires

Avant de composer, envoyer un courrier électronique
à [luc.maranget@inria.fr](mailto:luc.maranget@inria.fr?subject=MPRI.2.37.1,exam)
pour confirmer votre participation. Cela nous permettra de réagir si
nous ne recevons pas votre copie en temps et en heure.

Rester connecté(e) à la
[visioconférence](https://bbb-front.math.univ-paris-diderot.fr/ext/luc-d6m-2hw-699)
du cours pendant toute la durée de l'examen, afin de bénéficier
d'éventuelles annonces. En cas de question pendant l'examen, nous les
poser par courrier électronique à
[luc.maranget@inria.fr,
adrien@guatto.org](mailto:luc.maranget@inria.fr,adrien@guatto.org).
Les réponses susceptibles d'intéresser l'ensemble des étudiants seront
données par message oral et sur le *chat* de la visioconférence.
