MPRI 2.37.1 Exam
================

Ceci est ma contribution pour
l'[examen](http://pauillac.inria.fr/~maranget/MPRI/exam-fr.pdf) du cours
[2.37.1 Programming shared memory multicore
machines](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-37-1) du
[MPRI (Master Parisien de Recherche en
Informatique)](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-37-1)

Les instructions relatives à l'examen se trouvent dans le fichier
[Instructions.md](Instructions.md) ou sur [le site web de Luc
Maranget](http://pauillac.inria.fr/~maranget/MPRI/mode-exam.html).

Ce dépôt Git restera privé jusqu'à la fin de l'examen, et aucun `commit` ne
sera effectué après celle-ci.

License
-------

(Pour ce que ça importe...)

Les [instructions](Instructions.md), le [sujet de
l'examen](http://pauillac.inria.fr/~maranget/MPRI/exam-fr.pdf) ainsi que tout
élément repris tel quel de l'un ou de l'autre de ces documents sont copyright
(C) 2020-2021 Luc Maranget et Adrien Guatto (tous droits réservés).

Tout le reste est placé dans le domaine publique: vous pouvez le redistribuer
et/ou le modifier selon les termes de la licence CC0 telle que publiée par la
_Creative Commons Corporation_, que ce soit la version 1.0 de la licence, ou (à
votre discrétion) n'importe quelle version ultérieure.

Ces documents sont distribués avec l'espor qu'ils soient utiles, mais SANS
AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIABILITÉ ou DE
CONFORMITÉ À UNE UTILISATION PARTICULIÈRE.
Voir la license CC0 pour plus de détail.

Vous devriez avoir reçu une [copie de la licence CC0](LICENSE.txt) avec ces
documents.
Si ce n'est pas le cas, rendez-vous à cette adresse:
[https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr](https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr)
